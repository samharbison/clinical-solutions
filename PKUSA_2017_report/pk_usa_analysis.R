####PK USA Reporting
date1 = "2017-01-01"
date2 = "2017-12-31"
setwd("~/Desktop/cs/PKUSA_2017_report/")
source("../pk_usa_functions.R")
status = read.csv("tblStatus.csv") #visit related data
demos = read.csv("tblDemo.csv") #demographic data


pk_data = demo_visit(demos, status)
###employee 
#VISIT RELATED
e_visit = sort_date(pk_data$Employees.Visits,date1, date2)
tot_empl_visit = visit_rel_table(e_visit, date1 = date1, date2 = date2)
#OUTCOMES
tot_empl_wellness = outcomes_table(e_visit)
#RISK LEVELS
empl_bprisk = compare_prev(pk_data$Employees.Visits, date1, date2, "BPRISK")
empl_cholrisk = compare_prev(pk_data$Employees.Visits, date1, date2, "CHOLRISK")
empl_bgrisk = compare_prev(pk_data$Employees.Visits, date1, date2, "BGRISK")
empl_bmirisk = compare_prev(pk_data$Employees.Visits, date1, date2, "BMIRISK")
empl_overrisk = compare_prev(pk_data$Employees.Visits, date1, date2, "OVERRISK")

###Spouse
s_visit = sort_date(pk_data$Spouses.Visits, date1, date2)
tot_spou_visit = visit_rel_table(s_visit, date1 = date1, date2 = date2, employ = F)
tot_spou_wellness = outcomes_table(s_visit)
#RISK LEVELS
spou_bprisk = compare_prev(pk_data$Spouses.Visits, date1, date2, "BPRISK")
spou_cholrisk = compare_prev(pk_data$Spouses.Visits, date1, date2, "CHOLRISK")
spou_bgrisk = compare_prev(pk_data$Spouses.Visits, date1, date2, "BGRISK")
spou_bmirisk = compare_prev(pk_data$Spouses.Visits, date1, date2, "BMIRISK")
spou_overrisk = compare_prev(pk_data$Spouses.Visits, date1, date2, "OVERRISK")

