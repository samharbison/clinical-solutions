---
title: "PK USA Report"
author: "Sam Harbison"
output: word_document
params:
  tbldemo: NA
  tblstatus: NA
  date1: NA
  date2: NA
---
```{r results='asis', echo=FALSE, include=FALSE,}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE)
library(knitr)
library(kableExtra)
library(dplyr)
#source("~/Desktop/ClincicalSolutions/PKUSA/pkusadata/pk_usa_functions.R")
source("~/Desktop/cs/pk_usa_functions.R")
```
# Employees

The following is the report for the employees from the PK USA database. 
```{r}
demo = read.csv(params$tbldemo$datapath)
status = read.csv(params$tblstatus$datapath)
date1 = params$date1
date2 = params$date2
pk_data = demo_visit(demo, status)
```


## Visit Related Totals

```{r}
e_visit = sort_date(pk_data$Employees.Visits,date1, date2)
tot_empl_visit = visit_rel_table(e_visit, date1 = date1, date2 = date2)
kable(tot_empl_visit)
```

## Outcomes Table
The following table is made up of the total number of the employees and the total number of visits that the employees had on-going monitoring of Blood Pressure (OUTBP), Blood Glucose (OUTBG), Total Cholesterol (OUTCHOL), Weight Management (OUTWEIGHT), and Wellness (WELLNESS)
```{r}
tot_empl_wellness = outcomes_table(e_visit)
kable(tot_empl_wellness)
```

## Risk Factor Counts
The following tables are counts of Employees at each Risk Level for their last visit of 2017 and 2016. The following tables are for Blood Pressure (BPRISK), Blood Glucose (BGRISK), Total Cholesterol (CHOLRISK), BMI (BMIRISK), and OVerweight (OVERRISK)

For each risk variable, I will be presenting two tables. The first is just the totals for each year. The second only looks at employees who had visits in 2016 AND 2017 and includes columns for employees who only had visits in 2017.

First, here is a table of visits in both years as well as number of employees who only had visits in one of the two years.

```{r}
totals_table_e = compare_prev(pk_data$Employees.Visits, date1, date2, "BPRISK")$DF3
kable(totals_table_e)
```

### Blood Pressure
```{r}
empl_bprisk = compare_prev(pk_data$Employees.Visits, date1, date2, "BPRISK")$DF
empl_bprisk2 = compare_prev(pk_data$Employees.Visits, date1, date2, "BPRISK")$DF2

kable(empl_bprisk)
kable(empl_bprisk2)

```

### Blood Glucose
```{r}
empl_bgrisk = compare_prev(pk_data$Employees.Visits, date1, date2, "BGRISK")$DF
empl_bgrisk2 = compare_prev(pk_data$Employees.Visits, date1, date2, "BGRISK")$DF2


kable(empl_bgrisk)
kable(empl_bgrisk2)


```

### Total Cholesterol
```{r}
empl_cholrisk = compare_prev(pk_data$Employees.Visits, date1, date2, "CHOLRISK")$DF
empl_cholrisk2 = compare_prev(pk_data$Employees.Visits, date1, date2, "CHOLRISK")$DF2

kable(empl_cholrisk)
kable(empl_cholrisk2)


```

### BMI
```{r}
empl_bmirisk = compare_prev(pk_data$Employees.Visits, date1, date2, "BMIRISK")$DF
empl_bmirisk2 = compare_prev(pk_data$Employees.Visits, date1, date2, "BMIRISK")$DF2

kable(empl_bmirisk)
kable(empl_bmirisk2)

```

### Overweight
```{r}
empl_overrisk = compare_prev(pk_data$Employees.Visits, date1, date2, "OVERRISK")$DF
empl_overrisk2 = compare_prev(pk_data$Employees.Visits, date1, date2, "OVERRISK")$DF2

kable(empl_overrisk)
kable(empl_overrisk2)


```

# Spouses

The following is the report for the spouses from the PK USA database. 
## Visit Related Totals

```{r}
s_visit = sort_date(pk_data$Spouses.Visits, date1, date2)
tot_spou_visit = visit_rel_table(s_visit, date1 = date1, date2 = date2, employ = F)
kable(tot_spou_visit)
```

## Outcomes Table
The following table is made up of the total number of the spouses and the total number of visits that the employees had on-going monitoring of Blood Pressure (OUTBP), Blood Glucose (OUTBG), Total Cholesterol (OUTCHOL), Weight Management (OUTWEIGHT), and Wellness (WELLNESS)
```{r}
tot_spou_wellness = outcomes_table(s_visit, employ = F)
kable(tot_spou_wellness)
```

## Risk Factor Counts
The following tables are counts of spouses at each Risk Level for their last visit of 2017 and 2016. The following tables are for Blood Pressure (BPRISK), Blood Glucose (BGRISK), Total Cholesterol (CHOLRISK), BMI (BMIRISK), and Overweight (OVERRISK)

For each risk variable, I will be presenting two tables. The first is just the totals for each year. The second only looks at spouses who had visits in 2016 AND 2017 and includes columns for spouses who only had visits in 2017.

First, here is a table of visits in both years as well as number of spouses who only had visits in one of the two years.

```{r}
totals_table_s = compare_prev(pk_data$Spouses.Visits, date1, date2, "BPRISK")$DF3
kable(totals_table_s)

```

### Blood Pressure
```{r}
spou_bprisk = compare_prev(pk_data$Spouses.Visits, date1, date2, "BPRISK")$DF
spou_bprisk2 = compare_prev(pk_data$Spouses.Visits, date1, date2, "BPRISK")$DF2

kable(spou_bprisk)
kable(spou_bprisk2)

```

### Blood Glucose
```{r}
spou_bgrisk = compare_prev(pk_data$Spouses.Visits, date1, date2, "BGRISK")$DF
spou_bgrisk2 = compare_prev(pk_data$Spouses.Visits, date1, date2, "BGRISK")$DF2

kable(spou_bgrisk)
kable(spou_bgrisk2)


```

### Total Cholesterol
```{r}
spou_cholrisk = compare_prev(pk_data$Spouses.Visits, date1, date2, "CHOLRISK")$DF
spou_cholrisk2 = compare_prev(pk_data$Spouses.Visits, date1, date2, "CHOLRISK")$DF2

kable(spou_cholrisk)
kable(spou_cholrisk2)


```

### BMI
```{r}
spou_bmirisk = compare_prev(pk_data$Spouses.Visits, date1, date2, "BMIRISK")$DF
spou_bmirisk2 = compare_prev(pk_data$Spouses.Visits, date1, date2, "BMIRISK")$DF2

kable(spou_bmirisk)
kable(spou_bmirisk2)


```

### Overweight
```{r}
spou_overrisk = compare_prev(pk_data$Spouses.Visits, date1, date2, "OVERRISK")$DF
spou_overrisk2 = compare_prev(pk_data$Spouses.Visits, date1, date2, "OVERRISK")$DF2

kable(spou_overrisk)
kable(spou_overrisk2)


```
